# rkDomain

Script Bash para crear un archivo de configuración de proxy nginx + nodejs.

--

Primer script tocando Bash, habrá muchos fallos lo más probable y no será mejor forma de hacerlo.

--

+ Crea/elimina un archivo de configuración proxy en NGINX y una app NodeJS.
+ Crea/elimina el usuario sFTP
+ Crea/elimina la carpeta /var/www/%usuario% del usutio sFTP.
+ Crea una plantilla básica de app NodeJS + ExpressJS asignando el puerto.
+ Crea/elimina un usuario y base de datos.
+ Habilitar/deshabilitar SSL.

# Comandos

## Crear dominio

### Crear dominio sin DB
sudo ./rkDomain --create ejemplo.rayko.me 3000

### Crear dominio con DB
sudo ./rkDomain --create ejemplo.rayko.me 3000 --createDB

## Eliminar dominio
sudo ./rkDomain --delete ejemplo.rayko.me

### Eliminar dominio y DB
sudo ./rkDomain --delete ejemplo.rayko.me --deleteDB

## Listar dominios
sudo ./rkDomain --list

## Ejecutar APP
sudo ./rkDomain --run ejemplo.rayko.me

## Crear DB
sudo ./rkDomain --createDB ejemplo.rayko.me

## Borrar DB
sudo ./rkDomain --deleteDB ejemplo.rayko.me

## Habilitar SSL
sudo ./rkDomain --createDB ejemplo.rayko.me

## Deshabilitar SSL
sudo ./rkDomain --deleteDB ejemplo.rayko.me

# Bugs
[ ] Se crean \n al habilitar/deshabilitar SSL.

# To-Do
[ ] Registrar el SSL automáticamente con el CertBot